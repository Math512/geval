
## 1.21.0.0

* Add Probabilistic-MultiLabel-F-measure

## 1.20.1.0

* Fix Soft2D-F1 metric
* Check for invalid rectangles in Soft2D-F1 metric

## 1.20.0.0

* Add --list-metrics options
* Add Soft2D-F1 metric.

## 1.19.0.0

* Fully static build
* Add preprocessing options for metrics

## 1.18.2.0

* During validation, check the number of columns
* During validation, check the number of lines
* Validate train files

## 1.18.1.0

* During validation, check whether the maximum values is obtained with the expected data

## 1.18.0.0

* Add --validate option

## 1.17.0.0

* Add Probabilistic-Soft-F-score

## 1.16.0.0

* Handle JSONL files (only for MultiLabel-F-score)
* Fix SMAPE metric

## 1.0.0.1

* Added `--version`, `-v` options handling
